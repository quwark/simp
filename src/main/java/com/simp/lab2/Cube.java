package com.simp.lab2;


import java.util.List;

public interface Cube {

    List<List<Value>> getRows();

    List<Integer> getIndexes();
}
