package com.simp.lab2;

import com.simp.lab2.graph.Node;

import java.util.List;

public class CubeCreator {

    public static DCube createPrimitiveDCube(SchemaTestInfo testInfo, Value defect) {
        if(defect != Value.ONE && defect != Value.ZERO) {
            throw new IllegalArgumentException("Value invalid");
        }

        Value value;
        Value d;
        if(defect == Value.ZERO)  {
            value = Value.ONE;
            d = Value.D;
        } else {
            value = Value.ZERO;
            d = Value.RD;
        }

        Node testNode = testInfo.getTestNode();
        List<List<Value>> inputValues = testNode.getInputValues(value, d);

        return new DCube(testNode.getIndexes(), inputValues);
    }
}
