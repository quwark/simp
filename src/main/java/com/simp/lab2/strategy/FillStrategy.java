package com.simp.lab2.strategy;

import com.simp.lab2.Value;

import java.util.List;

public interface FillStrategy {

    List<List<Value>> getFillValues(int size, Value value);
}
