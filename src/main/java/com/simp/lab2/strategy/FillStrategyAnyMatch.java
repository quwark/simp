package com.simp.lab2.strategy;


import com.simp.lab2.Value;

import java.util.ArrayList;
import java.util.List;

public class FillStrategyAnyMatch implements FillStrategy {

    @Override
    public List<List<Value>> getFillValues(int size, Value value) {
        List<List<Value>> rows = new ArrayList<>();
        for (int j = 0; j < size; j++) {
            List<Value> resValues = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                if(i == j) {
                    resValues.add(value);
                } else {
                    resValues.add(Value.X);
                }
            }
            rows.add(resValues);
        }

        return rows;
    }
}
