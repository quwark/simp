package com.simp.lab2.strategy;

import com.simp.lab2.Value;

import java.util.ArrayList;
import java.util.List;

public class FillStrategyAllMatch implements FillStrategy {
    @Override
    public List<List<Value>> getFillValues(int size, Value value) {
        List<List<Value>> rows = new ArrayList<>();
        List<Value> values = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            values.add(value);
        }
        rows.add(values);
        return rows;
    }
}
