package com.simp.lab2;

import com.simp.lab2.graph.FunctionalNode;
import com.simp.lab2.graph.InputNode;
import com.simp.lab2.graph.Node;
import com.simp.lab2.op.Operand;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Schema {

    private List<Node> nodes = new ArrayList<>();
    private List<InputNode> inputNodes = new ArrayList<>();
    private List<FunctionalNode> functionalNodes = new ArrayList<>();

    private int inputNodeCount = 0;
    private int functionalNodeCount = 0;

    public InputNode createInputNode() {
        InputNode node = new InputNode(getIndex(), ++inputNodeCount);
        nodes.add(node);
        inputNodes.add(node);
        return node;
    }

    public FunctionalNode createFunctionalNode(Operand operand, Node ...inputs) {
        FunctionalNode node = new FunctionalNode(getIndex(), ++functionalNodeCount, operand, Arrays.asList(inputs));
        functionalNodes.add(node);
        nodes.add(node);
        return node;
    }

    public FunctionalNode createFunctionalNode(Node ...inputs) {
        return createFunctionalNode(Operand.NONE, inputs);
    }

    public Node getEnd() {
        return nodes.get(nodes.size() - 1);
    }

    public SchemaTestInfo getSchemaTestInfo(Node node) {
        List<Node> nodesForDCube = getNodesForDCube(node);
        List<Node> nodesForSCube = functionalNodes.stream()
                .filter(n -> !nodesForDCube.contains(n) && !node.equals(n))
                .collect(Collectors.toList());

        return new SchemaTestInfo(node, nodesForSCube, nodesForDCube);
    }

    private List<Node> getNodesForDCube(Node f3) {
        List<Node> nodes = new ArrayList<>();
        Node node =  f3.getNext();
        while (node != null) {
            nodes.add(node);
            node = node.getNext();
        }
        return nodes;
    }

    public int getInputNodeCount() {
        return inputNodeCount;
    }

    private int getIndex() {
        return inputNodeCount + functionalNodeCount;
    }

    public TestSuiteFinder createTable()  {
        return new TestSuiteFinder(nodes.size());
    }

    public SequenceCreator getSequenceCreator() {
        return new SequenceCreator(inputNodes.stream().map(InputNode::getIndex).collect(Collectors.toList()));
    }

    public boolean calculateValue(int input) {
        Node end = getEnd();
        return calculateValue(end, input);
    }

    private boolean calculateValue(Node node, int input) {
        if(node instanceof InputNode) {
            int index = ((InputNode) node).getNumber() - 1;
            return ((input >> index) & 1) == 1;
        } else if(node instanceof FunctionalNode) {
            List<Node> inputs = ((FunctionalNode) node).getInputs();
            Operand operand = ((FunctionalNode) node).getOperand();
            List<Boolean> collect = inputs.stream().map(i -> calculateValue(i, input)).collect(Collectors.toList());
            return operand.calculate(collect);
        }
        throw new IllegalArgumentException("Node");
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public List<FunctionalNode> getFunctionalNodes() {
        return functionalNodes;
    }

    public List<NodeResult> getNodeResult() {
        List<NodeResult> nodeResults = new ArrayList<>();
        for (Node node : functionalNodes) {
            nodeResults.addAll(test(this, node));
        }
        return nodeResults;
    }

    public Set<Integer> getUniqueInputs() {
        return getUniqueIntegers(getNodeResult());
    }

    public Set<String> getUniqueStringInputs() {
        return getUniqueStrings(getNodeResult());
    }


    public static List<NodeResult> test(Schema schema, Node testNode) {

        SequenceCreator sequenceCreator = schema.getSequenceCreator();
        SchemaTestInfo testInfo = schema.getSchemaTestInfo(testNode);

        List<Value> valueRow0 = getValueRow(schema, testInfo, Value.ZERO);
        List<Value> valueRow1 = getValueRow(schema, testInfo, Value.ONE);

        NodeResult nodeResult0 = new NodeResult(testNode, Value.ZERO, sequenceCreator.getExpected(valueRow0),
                sequenceCreator.createAsBitInt(valueRow0), sequenceCreator.createAsString(valueRow0), valueRow0);
        NodeResult nodeResult1 = new NodeResult(testNode, Value.ONE,
                sequenceCreator.getExpected(valueRow1), sequenceCreator.createAsBitInt(valueRow1),
                sequenceCreator.createAsString(valueRow1),
                valueRow1);

        return Stream.of(nodeResult0, nodeResult1).collect(Collectors.toList());
    }

    private static List<Value> getValueRow(Schema schema, SchemaTestInfo testInfo, Value defect) {
        TestSuiteFinder testSuiteFinder = schema.createTable();

        return testSuiteFinder.process(testInfo, defect);
    }

    private static Set<Integer> getUniqueIntegers(List<NodeResult> nodeResults) {
        Map<Integer, List<NodeResult>> map = new HashMap<>();

        for (int i = 0; i < nodeResults.size(); i++) {
            NodeResult nodeResult = nodeResults.get(i);
            for (int j = 0; j < nodeResult.getInputs().size(); j++) {
                Integer value = nodeResult.getInputs().get(j);
                List<NodeResult> nodes = map.get(value);
                if(nodes == null) {
                    List<NodeResult> nodeResultList = new ArrayList<>();
                    nodeResultList.add(nodeResult);
                    map.put(value, nodeResultList);
                } else {
                    nodes.add(nodeResult);
                }
            }
        }

        Set<Integer> inputsToTestAllDefects = new HashSet<>();
        Set<NodeResult> nodeResultSet = new HashSet<>();

        List<Map.Entry<Integer, List<NodeResult>>> values = map
                .entrySet()
                .stream()
                .sorted((entry1, entry2) -> -Integer.compare(entry1.getValue().size(), entry2.getValue().size()))
                .collect(Collectors.toList());

        for (int i = 0; i < values.size(); i++) {
            if(nodeResultSet.size() == nodeResults.size()) {
                break;
            }
            Map.Entry<Integer, List<NodeResult>> integerListEntry = values.get(i);
            for (int j = 0; j < integerListEntry.getValue().size(); j++) {
                NodeResult nodeResult = integerListEntry.getValue().get(j);
                if(!nodeResultSet.contains(nodeResult)) {
                    nodeResultSet.add(nodeResult);
                    inputsToTestAllDefects.add(integerListEntry.getKey());
                }
                if(nodeResultSet.size() == nodeResults.size()) {
                    break;
                }
            }
        }
        return inputsToTestAllDefects;
    }

    private static Set<String> getUniqueStrings(List<NodeResult> nodeResults) {
        Map<String, List<NodeResult>> map = new HashMap<>();

        for (int i = 0; i < nodeResults.size(); i++) {
            NodeResult nodeResult = nodeResults.get(i);
            for (int j = 0; j < nodeResult.getInputs().size(); j++) {
                String value = nodeResult.getStrings().get(j);
                List<NodeResult> nodes = map.get(value);
                if(nodes == null) {
                    List<NodeResult> nodeResultList = new ArrayList<>();
                    nodeResultList.add(nodeResult);
                    map.put(value, nodeResultList);
                } else {
                    nodes.add(nodeResult);
                }
            }
        }

        Set<String> inputsToTestAllDefects = new HashSet<>();
        Set<NodeResult> nodeResultSet = new HashSet<>();

        List<Map.Entry<String, List<NodeResult>>> values = map
                .entrySet()
                .stream()
                .sorted((entry1, entry2) -> -Integer.compare(entry1.getValue().size(), entry2.getValue().size()))
                .collect(Collectors.toList());

        for (int i = 0; i < values.size(); i++) {
            if(nodeResultSet.size() == nodeResults.size()) {
                break;
            }
            Map.Entry<String, List<NodeResult>> integerListEntry = values.get(i);
            for (int j = 0; j < integerListEntry.getValue().size(); j++) {
                NodeResult nodeResult = integerListEntry.getValue().get(j);
                if(!nodeResultSet.contains(nodeResult)) {
                    nodeResultSet.add(nodeResult);
                    inputsToTestAllDefects.add(integerListEntry.getKey());
                }
                if(nodeResultSet.size() == nodeResults.size()) {
                    break;
                }
            }
        }
        return inputsToTestAllDefects;
    }
}
