package com.simp.lab2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SCube implements Cube {

    private List<Integer> indexes;
    private List<List<Value>> rows0;
    private List<List<Value>> rows1;

    public SCube(List<Integer> indexes, List<List<Value>> rows0, List<List<Value>> rows1) {
        this.indexes = indexes;
        this.rows0 = rows0;
        this.rows1 = rows1;
    }

    public DCube getDCube(List<Integer> indexes) {
        List<List<Value>> dCube = new ArrayList<>();
        for (List<Value> aRows0 : rows0) {
            for (List<Value> aRows1 : rows1) {
                dCube.add(intersect(aRows0, aRows1));
            }
        }

        for (List<Value> aRows1 : rows1) {
            for (List<Value> aRows0 : rows0) {
                dCube.add(intersect(aRows1, aRows0));
            }
        }

        return new DCube(indexes, dCube);
    }

    private List<Value> intersect(List<Value> val1, List<Value> val2) {
        List<Value> values = new ArrayList<>();
        for (int i = 0; i < val1.size(); i++) {
            values.add(intersect(val1.get(i), val2.get(i)));

        }
        return values;
    }

    private Value intersect(Value val1, Value val2) {
        String key = val1.name() + val2.name();
        return opMap.get(key);
    }

    public List<Integer> getIndexes() {
        return indexes;
    }

    private static final Map<String, Value> opMap = new HashMap<>();

    static {
        opMap.put(Value.ZERO.name() + Value.ZERO.name(), Value.ZERO);
        opMap.put(Value.ZERO.name() + Value.X.name(), Value.ZERO);
        opMap.put(Value.X.name() + Value.ZERO.name(), Value.ZERO);

        opMap.put(Value.X.name() + Value.X.name(), Value.X);

        opMap.put(Value.ONE.name() + Value.ONE.name(), Value.ONE);
        opMap.put(Value.ONE.name() + Value.X.name(), Value.ONE);
        opMap.put(Value.X.name() + Value.ONE.name(), Value.ONE);

        opMap.put(Value.ONE.name() + Value.ZERO.name(), Value.D);
        opMap.put(Value.ZERO.name() + Value.ONE.name(), Value.RD);
    }

    @Override
    public String toString() {
        return "SCube{" +
                "rows0=" + rows0 +
                ", rows1=" + rows1 +
                '}';
    }

    public List<List<Value>> getRows() {
        List<List<Value>> rows = new ArrayList<>(rows0);
        rows.addAll(rows1);
        return rows;
    }
}
