package com.simp.lab2;

import com.simp.lab2.graph.Node;

import java.util.List;

public class SchemaTestInfo {

    private final Node testNode;
    private final List<Node> nodesOutOfTheWay;
    private final List<Node> nodesInFront;

    public SchemaTestInfo(Node testNode, List<Node> nodesOutOfTheWay, List<Node> nodesInFront) {
        this.nodesOutOfTheWay = nodesOutOfTheWay;
        this.nodesInFront = nodesInFront;
        this.testNode = testNode;
    }

    public List<Node> getNodesOutOfTheWay() {
        return nodesOutOfTheWay;
    }

    public List<Node> getNodesInFront() {
        return nodesInFront;
    }


    public Node getTestNode() {
        return testNode;
    }

    @Override
    public String toString() {
        return "SchemaTestInfo{" +
                "testNode=" + testNode +
                ", sCubeNodes=" + nodesOutOfTheWay +
                ", dCubeNodes=" + nodesInFront +
                '}';
    }
}
