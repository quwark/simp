package com.simp.lab2;

import com.simp.lab2.graph.FunctionalNode;
import com.simp.lab2.graph.Node;

import java.util.List;

public class NodeResult {

    private Node node;
    private Value defect;
    private List<Integer> inputs;
    private List<String> strings;
    private List<Value> values;
    private Value expected;

    public NodeResult(Node node, Value defect, Value expected, List<Integer> inputs, List<String> strings, List<Value> values) {
        this.node = node;
        this.defect = defect;
        this.inputs = inputs;
        this.values = values;
        this.expected = expected;
        this.strings = strings;
    }

    public Node getNode() {
        return node;
    }

    public Value getDefect() {
        return defect;
    }

    public List<Integer> getInputs() {
        return inputs;
    }

    public List<Value> getValues() {
        return values;
    }

    public Value getExpected() {
        return expected;
    }

    public List<String> getStrings() {
        return strings;
    }
}
