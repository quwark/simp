package com.simp.lab2;

import java.util.List;

public class DCube implements Cube {

    private List<Integer> indexes;
    private List<List<Value>> rows;

    public DCube(List<Integer> indexes, List<List<Value>> rows) {
        this.indexes = indexes;
        this.rows = rows;
    }

    public List<Integer> getIndexes() {
        return indexes;
    }

    public List<List<Value>> getRows() {
        return rows;
    }

    @Override
    public String toString() {
        return "DCube{" +
                "rows=" + rows +
                '}';
    }
}
