package com.simp.lab2.op;

import com.simp.lab2.Value;

import java.util.List;

public interface InputFinder {

    List<List<Value>> getInputsFor(int size, Value value);
}
