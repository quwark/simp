package com.simp.lab2.op;

import com.simp.lab2.Value;
import com.simp.lab2.strategy.FillStrategy;
import com.simp.lab2.strategy.FillStrategyAllMatch;
import com.simp.lab2.strategy.FillStrategyAnyMatch;

import java.util.List;

public enum Operand implements Calculator, InputFinder {

    NONE {
        @Override
        public boolean calculate(List<Boolean> inputs) {
            if(inputs.size() != 1) {
                throw new IllegalArgumentException("Invalid inputs size");
            }
            return inputs.get(0);
        }

        @Override
        public List<List<Value>> getInputsFor(int size, Value value) {
            return matchAll.getFillValues(size, value);
        }
    },

    ONE {
        @Override
        public boolean calculate(List<Boolean> inputs) {
            return true;
        }

        @Override
        public List<List<Value>> getInputsFor(int size, Value value) {
            return matchAll.getFillValues(size, value);
        }
    },

    ZERO {
        @Override
        public boolean calculate(List<Boolean> inputs) {
            return false;
        }

        @Override
        public List<List<Value>> getInputsFor(int size, Value value) {
            return matchAll.getFillValues(size, value);
        }
    },

    NOT {
        @Override
        public boolean calculate(List<Boolean> inputs) {
            if(inputs.size() != 1) {
                throw new IllegalArgumentException("Invalid inputs size");
            }
            return !inputs.get(0);
        }

        @Override
        public List<List<Value>> getInputsFor(int size, Value value) {
            return matchAll.getFillValues(size, value == Value.ZERO ? Value.ONE : Value.ZERO);
        }
    },

    OR {
        @Override
        public boolean calculate(List<Boolean> inputs) {
            boolean result = inputs.get(0);
            for (int i = 1; i < inputs.size(); i++) {
                result |= inputs.get(i);
            }
            return result;
        }

        @Override
        public List<List<Value>> getInputsFor(int size, Value value) {
            if(value == Value.ZERO) {
                return new FillStrategyAllMatch().getFillValues(size, value);
            } else if(value == Value.ONE) {
                return matchAny.getFillValues(size, value);
            }
            throw new IllegalArgumentException("Or");
        }
    },

    AND {
        @Override
        public boolean calculate(List<Boolean> inputs) {
            boolean result = inputs.get(0);
            for (int i = 1; i < inputs.size(); i++) {
                result &= inputs.get(i);
            }
            return result;
        }

        @Override
        public List<List<Value>> getInputsFor(int size, Value value) {
            if(value == Value.ZERO) {
                return matchAny.getFillValues(size, value);
            } else if(value == Value.ONE) {
                return matchAll.getFillValues(size, value);
            }
            throw new IllegalArgumentException("And");
        }
    },

    OR_NOT {
        @Override
        public boolean calculate(List<Boolean> inputs) {
            return !OR.calculate(inputs);
        }

        @Override
        public List<List<Value>> getInputsFor(int size, Value value) {
            if(value == Value.ONE) {
                return matchAll.getFillValues(size, Value.ZERO);
            } else if(value == Value.ZERO) {
                return matchAny.getFillValues(size, Value.ONE);
            }
            throw new IllegalArgumentException("Or Not");
        }
    },

    AND_NOT {
        @Override
        public boolean calculate(List<Boolean> inputs) {
            return !AND.calculate(inputs);
        }

        @Override
        public List<List<Value>> getInputsFor(int size, Value value) {
            if(value == Value.ONE) {
                return matchAny.getFillValues(size,  Value.ZERO);
            } else if(value == Value.ZERO) {
                return matchAll.getFillValues(size, Value.ONE);
            }
            throw new IllegalArgumentException("And Not");
        }
    };

    private static final FillStrategy matchAll = new FillStrategyAllMatch();
    private static final FillStrategy matchAny = new FillStrategyAnyMatch();
}
