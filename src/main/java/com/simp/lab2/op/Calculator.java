package com.simp.lab2.op;


import java.util.List;

public interface Calculator {

    boolean calculate(List<Boolean> inputs);
}
