package com.simp.lab2;

import com.simp.lab2.graph.FunctionalNode;
import com.simp.lab2.graph.InputNode;
import com.simp.lab2.graph.Node;
import com.simp.lab2.op.Operand;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Runner {

    public static void main(String[] args) {
        Schema schema = new Schema();

        //Tester example

        /*InputNode x1 = schema.createInputNode();
        InputNode x2 = schema.createInputNode();
        InputNode x3 = schema.createInputNode();
        InputNode x4 = schema.createInputNode();
        InputNode x5 = schema.createInputNode();
        InputNode x6 = schema.createInputNode();

        FunctionalNode f1 = schema.createFunctionalNode(Operand.AND, x1, x2);
        FunctionalNode f2 = schema.createFunctionalNode(Operand.OR_NOT, x4, x5);
        FunctionalNode f3 = schema.createFunctionalNode(Operand.AND_NOT, f2, x6);
        FunctionalNode f4 = schema.createFunctionalNode(Operand.AND, x3, f3);
        FunctionalNode f5 = schema.createFunctionalNode(Operand.OR, f1, f4);*/


        /*System.out.println(schema.getSchemaTestInfo(f3));
        System.out.println(f3.getOperand().getInputsFor(f3.getInputs().size(), Value.ONE));
        System.out.println(Operand.OR_NOT.getInputsFor(3, Value.ONE));
        System.out.println(Operand.OR_NOT.getInputsFor(3, Value.ZERO));*/

        InputNode x1 = schema.createInputNode();
        InputNode x2 = schema.createInputNode();
        InputNode x3 = schema.createInputNode();
        InputNode x4 = schema.createInputNode();
        InputNode x5 = schema.createInputNode();
        InputNode x6 = schema.createInputNode();
        InputNode x7 = schema.createInputNode();

        /*FunctionalNode x1 = schema.createFunctionalNode(x11);
        FunctionalNode x2 = schema.createFunctionalNode(x22);
        FunctionalNode x3 = schema.createFunctionalNode(x33);
        FunctionalNode x4 = schema.createFunctionalNode(x44);
        FunctionalNode x5 = schema.createFunctionalNode(x55);
        FunctionalNode x6 = schema.createFunctionalNode(x66);
        FunctionalNode x7 = schema.createFunctionalNode(x77);*/

        FunctionalNode f1 = schema.createFunctionalNode(Operand.AND, x1, x2);
        FunctionalNode f2 = schema.createFunctionalNode(Operand.NOT, x3);
        FunctionalNode f3 = schema.createFunctionalNode(Operand.OR, x5, x6);
        FunctionalNode f4 = schema.createFunctionalNode(Operand.AND_NOT, x4, f3, x7); // AND_NOT
        FunctionalNode f5 = schema.createFunctionalNode(Operand.OR_NOT, f2, f4); //OR_NOT
        FunctionalNode f6 = schema.createFunctionalNode(Operand.AND, f1, f5);

       /* FunctionalNode f1 = schema.createFunctionalNode(Operand.AND_NOT, x1, x2);
        FunctionalNode f2 = schema.createFunctionalNode(Operand.NOT, x3);
        FunctionalNode f3 = schema.createFunctionalNode(Operand.OR_NOT, x5, x6);
        FunctionalNode f4 = schema.createFunctionalNode(Operand.AND, x4, f3, x7); // AND_NOT
        FunctionalNode f5 = schema.createFunctionalNode(Operand.OR, f2, f4); //OR_NOT
        FunctionalNode f6 = schema.createFunctionalNode(Operand.AND, f1, f5);*/

        /*FunctionalNode f1 = schema.createFunctionalNode(Operand.OR_NOT, x1, x2);
        FunctionalNode f2 = schema.createFunctionalNode(Operand.NOT, x3);
        FunctionalNode f3 = schema.createFunctionalNode(Operand.AND, x5, x6);
        FunctionalNode f4 = schema.createFunctionalNode(Operand.AND_NOT, x4, f3, x7); // AND_NOT
        FunctionalNode f5 = schema.createFunctionalNode(Operand.OR, f2, f4); //OR_NOT
        FunctionalNode f6 = schema.createFunctionalNode(Operand.OR, f1, f5);*/

        List<Node> functionalNodes = Arrays.asList(f1, f2, f3, f4, f5, f6);
        //List<FunctionalNode> functionalNodes = Arrays.asList(f3);
        List<NodeResult> nodeResults = schema.getNodeResult();

        System.out.println("---------------------------Test sequence-----------------------------");

        nodeResults.sort(Comparator.comparing(nodeResult2 -> nodeResult2.getNode().toString()));
        for (NodeResult nodeResult : nodeResults) {
            System.out.println("Node: " + nodeResult.getNode());
            System.out.println("------Defect Const " + nodeResult.getDefect());
            nodeResult.getStrings().forEach(System.out::println);


        }

        System.out.println("Tester node results ==============================");

        //f3.setOperand(Operand.ONE);

        System.out.println("Unique defects = " + schema.getUniqueStringInputs());

        for (NodeResult nodeResult : nodeResults) {
            nodeResult.getInputs().forEach(input -> {
                boolean calculate = schema.calculateValue(input);
                boolean expected = nodeResult.getExpected() != Value.ZERO;
                if (calculate == expected) {
                    System.out.println(nodeResult.getDefect() + " Tester seq for " + nodeResult.getNode().toString() + " success" + input);
                } else {
                    System.out.println(nodeResult.getDefect() + "  Tester seq for " + nodeResult.getNode().toString() + " failed " + input + " values == " + nodeResult.getValues());
                }
            });

        }

    }
}
