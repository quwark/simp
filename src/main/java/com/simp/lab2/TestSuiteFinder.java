package com.simp.lab2;

import com.simp.lab2.graph.FunctionalNode;
import com.simp.lab2.graph.Node;

import java.util.*;
import java.util.stream.Collectors;

public class TestSuiteFinder {

    private List<Value> values;
    private Deque<HistoryItem> history = new LinkedList<>();
    private Deque<Cube> cubesToProcess = new LinkedList<>();
    private int size;

    public TestSuiteFinder(int size) {
       this.size = size;
       this.values = getNewRow(size);
     }


    public void intersect(Cube cube) {
        history.push(new HistoryItem(values, cube, 0));

        List<Value> fullRow = getFullRow(cube.getRows().get(0), cube.getIndexes());
        this.values = intersect(this.values, fullRow);
    }

    public List<Value> process(SchemaTestInfo testInfo, Value defect) {

        testInfo.getNodesOutOfTheWay().stream().map(Node::getSCube).forEach(node -> cubesToProcess.push(node));
        testInfo.getNodesInFront().stream().map(Node::getDCube).forEach(node -> cubesToProcess.push(node));

        intersect(CubeCreator.createPrimitiveDCube(testInfo, defect));

        while (!cubesToProcess.isEmpty()) {
            intersect(cubesToProcess.pop());
            while (!isValid()) {
                reIntersect();
            }
        }

        return getValues();
    }

    public boolean isValid() {
        return values.stream().noneMatch(Objects::isNull);
    }

    public void reIntersect() {
        if(history.isEmpty()) {
            return;
        }

        HistoryItem pop = history.pop();
        this.values = pop.valueRow;
        int index = pop.index + 1;
        Cube cube = pop.cube;

        if(cube.getRows().size() == index) {
            cubesToProcess.push(cube);
            reIntersect();
            return;
        }

        history.push(new HistoryItem(this.values, cube, index));

        List<Value> fullRow = getFullRow(cube.getRows().get(index), cube.getIndexes());
        this.values = intersect(this.values, fullRow);
    }

    private List<Value> getFullRow(List<Value> row, List<Integer> indexes) {
         List<Value> newRow = getNewRow();
         for (int i = 0; i < indexes.size(); i++) {
             newRow.set(indexes.get(i), row.get(i));
         }

         return newRow;
    }

    private List<Value> intersect(List<Value> a, List<Value> b) {
        List<Value> values = new ArrayList<>();
        for (int i = 0; i < a.size(); i++) {
            values.add(intersect(a.get(i), b.get(i)));
        }

        return values;
    }

    private Value intersect(Value a, Value b) {
        if(a == b || b == Value.X) {
            return a;
        }

        if(a == Value.X) {
            return b;
        }

        return null;
    }

    public List<Value> getValues() {
        return values;
    }

    private List<Value> getNewRow() {
        return getNewRow(size);
    }

     private List<Value> getNewRow(int size) {
         List<Value> values = new ArrayList<>(size);
         for (int i = 0; i < size; i++) {
             values.add(Value.X);
         }
         return values;
     }

     private static class HistoryItem {
        private List<Value> valueRow;
        private Cube cube;
        private int index;

         public HistoryItem(List<Value> valueRow, Cube cube, int index) {
             this.valueRow = valueRow;
             this.cube = cube;
             this.index = index;
         }

     }
}
