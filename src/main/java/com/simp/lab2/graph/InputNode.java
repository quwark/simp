package com.simp.lab2.graph;

import com.simp.lab2.DCube;
import com.simp.lab2.SCube;
import com.simp.lab2.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputNode implements Node {

    private final int index;
    private final int number;
    private Node next;

    public InputNode(int index, int number) {
        this.index = index;
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public Node getNext() {
        return next;
    }

    @Override
    public void setNext(Node next) {
        this.next = next;
    }

    @Override
    public int getIndex() {
        return index;
    }

    public List<List<Value>> getInputValues(Value value) {
        return getInputValues(value, value);
    }

    @Override
    public SCube getSCube() {
        return new SCube(getIndexes(), getInputValues(Value.ZERO), getInputValues(Value.ONE));
    }

    @Override
    public DCube getDCube() {
        return null;
    }

    @Override
    public List<List<Value>> getInputValues(Value value, Value valueToSet) {
        List<List<Value>> inputValues = new ArrayList<>();
        List<Value> valueList = new ArrayList<>();
        valueList.add(value);
        inputValues.add(valueList);
        inputValues.forEach(l -> l.add(valueToSet));
        return inputValues;
    }

    @Override
    public List<Integer> getIndexes() {
        return Stream.of(index, next.getIndex()).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "x" + number + "(" + index + ")";
    }
}
