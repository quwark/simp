package com.simp.lab2.graph;

import com.simp.lab2.DCube;
import com.simp.lab2.SCube;
import com.simp.lab2.Value;

import java.util.List;

public interface Node {

    Node getNext();

    void setNext(Node next);

    int getIndex();

    SCube getSCube();

    DCube getDCube();

    List<Integer> getIndexes();

    List<List<Value>> getInputValues(Value value, Value valueToSet);
}
