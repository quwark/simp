package com.simp.lab2.graph;

import com.simp.lab2.DCube;
import com.simp.lab2.SCube;
import com.simp.lab2.Value;
import com.simp.lab2.op.Operand;

import java.util.ArrayList;
import java.util.List;

public class FunctionalNode implements Node {

    private int index;
    private int number;
    private Node next;
    private List<Node> inputs;
    private Operand operand;

    public FunctionalNode(int index, int number, Operand operand, List<Node> inputs) {
        this.index = index;
        this.number = number;
        this.inputs = inputs;
        this.operand = operand;
        inputs.forEach(node -> node.setNext(this));
    }

    public List<Node> getInputs() {
        return inputs;
    }

    public Operand getOperand() {
        return operand;
    }

    @Override
    public List<List<Value>> getInputValues(Value value, Value valueToSet) {
        List<List<Value>> inputValues = operand.getInputsFor(inputs.size(), value);
        inputValues.forEach(l -> l.add(valueToSet));
        return inputValues;
    }

    public List<List<Value>> getInputValues(Value value) {
        return getInputValues(value, value);
    }

    public SCube getSCube() {
        return new SCube(getIndexes(), getInputValues(Value.ZERO), getInputValues(Value.ONE));
    }

    public DCube getDCube() {
        return getSCube().getDCube(getIndexes());
    }

   @Override
    public List<Integer> getIndexes() {
        List<Integer> indexes = new ArrayList<>();
        inputs.forEach(input -> indexes.add(input.getIndex()));
        indexes.add(index);
        return indexes;
    }

    @Override
    public Node getNext() {
        return next;
    }

    @Override
    public void setNext(Node next) {
        this.next = next;
    }

    public void setOperand(Operand operand) {
        this.operand = operand;
    }

    @Override
    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "F" + number + "(" + index + ")";
    }

}
