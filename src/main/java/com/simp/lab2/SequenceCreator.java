package com.simp.lab2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SequenceCreator {

    private List<Integer> indexes;

    public SequenceCreator(List<Integer> indexes) {
        this.indexes = indexes;
    }

    public Value getExpected(List<Value> values) {
        if(values.get(values.size() - 1) == Value.D) {
            return Value.ONE;
        }
        return Value.ZERO;
    }

    public List<List<Value>> create(List<Value> values) {
        List<Value> inputs = indexes.stream().map(values::get).collect(Collectors.toList());
        return getInputSequences(inputs);
    }

    public List<String> createAsString(List<Value> values) {
        return create(values).stream().map(l -> {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < l.size(); i++) {
                sb.append(l.get(i) == Value.ZERO ? '0' : '1');
            }
            return sb.toString();
        }).collect(Collectors.toList());
    }

    public List<Integer> createAsBitInt(List<Value> values) {
        return create(values).stream().map(l -> {
            StringBuilder sb = new StringBuilder();
            for (int i = l.size() - 1; 0 <= i; i--) {
                sb.append(l.get(i) == Value.ZERO ? '0' : '1');
            }
            return Integer.parseInt(sb.toString(), 2);
        }).collect(Collectors.toList());
    }

    private List<List<Value>> getInputSequences(List<Value> inputs) {
        List<Integer> idx = new ArrayList<>();
        for (int i = 0; i < inputs.size(); i++) {
            if(inputs.get(i) == Value.X) {
                idx.add(i);
            }
        }

        if(idx.isEmpty()) {
            return Collections.singletonList(inputs);
        }

        int xCount = idx.size();
        int value = 1 << xCount;

        List<List<Value>> result = new ArrayList<>();
        for (int i = 0; i < value; i++) {
            List<Value> values = new ArrayList<>(inputs);
            for (int j = 0; j < idx.size(); j++) {
                Value val = ((i >> j) & 1) == 0 ? Value.ZERO : Value.ONE;
                values.set(idx.get(j), val);
            }
            result.add(values);
        }
        return result;
    }
}
