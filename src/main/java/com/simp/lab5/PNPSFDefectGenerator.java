package com.simp.lab5;

import com.simp.lab4.memory.cell.Cell;
import com.simp.lab4.memory.defect.DefectGeneratorBase;
import com.simp.lab4.memory.defect.DefectType;

import java.util.ArrayList;
import java.util.List;

public class PNPSFDefectGenerator extends DefectGeneratorBase {

    private int k;

    public PNPSFDefectGenerator(int defectCount, int k) {
        super(defectCount);
        this.k = k;

        int minValue = 1 << (k - 1);
        if(defectCount < minValue) {
            throw new IllegalArgumentException("invalid K argument. Must be grater or equals then " + minValue);
        }
    }

    @Override
    protected List<DefectType> getDefectTypes() {
        return generateDefectTypes();
    }

    private List<DefectType> generateDefectTypes() {
        List<DefectType> defectTypes = new ArrayList<>();
        for (int i = 0; i < 1 << (k - 1); i++) {
            Integer val = i;
            defectTypes.add(((simpleMemory, addresses) -> {
                String s = toValue(val);
                for (int j = 0; j < addresses.size(); j += k) {
                    Cell cell = simpleMemory.get(addresses.get(j));
                    List<Cell> cells = new ArrayList<>();
                    for (int l = 0; l < k - 1; l++) {
                        cells.add(simpleMemory.get(addresses.get(j + l)));
                    }
                    simpleMemory.set(addresses.get(j), new PNPSFCell(cells, cell, s));
                }
            }));
        }

        return defectTypes;
    }

    private String toValue(int i) {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < (k - 1); j++) {
            sb.append((i >> j) & 1);
        }
        return sb.toString();
    }

    private static String toValue(List<Cell> cells) {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < cells.size(); j++) {
            sb.append(cells.get(j).read() ? 1 : 0);
        }
        return sb.toString();
    }

    @Override
    protected int getMultiplier() {
        return k;
    }

    @Override
    public String toString() {
        return "PNPSFK" + k;
    }

    private static class PNPSFCell implements Cell {

        private List<Cell> cells;
        private Cell cell;
        private String pattern;

        public PNPSFCell(List<Cell> cells, Cell cell, String pattern) {
            this.cells = cells;
            this.cell = cell;
            this.pattern = pattern;
        }

        @Override
        public boolean read() {
            return cell.read();
        }

        @Override
        public void write(boolean value) {
            String s = toValue(cells);
            if(!s.equals(pattern)) {
                cell.write(value);
            }
        }
    }
}
