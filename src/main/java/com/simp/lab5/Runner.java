package com.simp.lab5;

import com.simp.lab4.Constants;
import com.simp.lab4.MemoryTestTimeAnalyzer;
import com.simp.lab4.memory.Memory;
import com.simp.lab4.ResultView;
import com.simp.lab4.memory.SimpleMemory;
import com.simp.lab4.memory.defect.DefectGenerator;
import com.simp.lab4.test.MarchXTester;
import com.simp.lab4.test.Tester;

import java.util.Arrays;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        ResultView.show(getDefectGenerators(), getTesters());

        Memory memory = new SimpleMemory(60 * 1024 *  1024 );
        MemoryTestTimeAnalyzer testTimeAnalyzer = new MemoryTestTimeAnalyzer(1333_000_000);
        ResultView.show(memory, getTesters(), testTimeAnalyzer);
    }

    private static List<DefectGenerator> getDefectGenerators() {
        return Arrays.asList(new PNPSFDefectGenerator(1000, 5),
                new ANPSFDefectGenerator(1000, 9));
    }

    private static List<Tester> getTesters() {
        return Arrays.asList(new MarchXTester(), new MarchPSTester());
    }
}
