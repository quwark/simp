package com.simp.lab5;

import com.simp.lab4.memory.Memory;
import com.simp.lab4.test.Tester;

import java.util.HashSet;
import java.util.Set;

public class MarchPSTester implements Tester {

    @Override
    public Set<Integer> execute(Memory memory) {
        Set<Integer> defects = new HashSet<>();

        for (int i = 0; i < memory.getSize(); i++) {
            write(memory, i, false);
        }

        for (int i = 0; i < memory.getSize(); i++) {
            read(memory, i , false, defects);
            write(memory, i, true);
            read(memory, i,true, defects);
            write(memory, i, false);
            read(memory, i, false, defects);
            write(memory, i ,true);
        }

        for (int i = 0; i < memory.getSize(); i++) {
            read(memory, i, true, defects);
            write(memory, i, false);
            read(memory, i, false, defects);
            write(memory, i, true);
            read(memory, i, true, defects);
        }

        for (int i = 0; i < memory.getSize(); i++) {
            read(memory, i, true, defects);
            write(memory, i, false);
            read(memory, i, false, defects);
            write(memory, i, true);
            read(memory, i, true, defects);
            write(memory, i, false);
        }

        for (int i = 0; i < memory.getSize(); i++) {
            read(memory, i, false, defects);
            write(memory, i, true);
            read(memory, i, true, defects);
            write(memory, i, false);
            read(memory, i, false, defects);
        }

        return defects;
    }

    @Override
    public long getNumberOfOperations(long n) {
        return 23 * n;
    }

    private void write(Memory memory, int i, boolean value) {
        memory.get(i).write(value);
    }

    private void read(Memory memory, int i, boolean value, Set<Integer> defects) {
        if(memory.get(i).read() != value) {
            defects.add(i);
        }
    }

    @Override
    public String toString() {
        return "MarchPS";
    }
}
