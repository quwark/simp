package com.simp.lab5;

import com.simp.lab4.memory.Memory;
import com.simp.lab4.memory.cell.Cell;
import com.simp.lab4.memory.defect.DefectGeneratorBase;
import com.simp.lab4.memory.defect.DefectType;

import java.util.ArrayList;
import java.util.List;

public class ANPSFDefectGenerator extends DefectGeneratorBase {

    private int k;

    public ANPSFDefectGenerator(int defectCount, int k) {
        super(defectCount);
        this.k = k;

        int minValue = 1 << k;
        if(defectCount < minValue) {
            throw new IllegalArgumentException("invalid K argument. Must be grater or equals then " + minValue);
        }

    }

    public ANPSFDefectGenerator(int defectCount) {
        this(defectCount,  9);
    }

    @Override
    protected List<DefectType> getDefectTypes() {
        List<DefectType> defectTypes = new ArrayList<>();

        generateDefectForValue(defectTypes, false);
        generateDefectForValue(defectTypes, true);

        return defectTypes;
    }

    private void generateDefectForValue(List<DefectType> defectTypes, boolean value) {
        for (int i = 0; i < (1 << (k - 1)); i++) {
            Integer val = i;
            defectTypes.add(((simpleMemory, addresses) -> {
                String s = toValue(val);
                generateDefect(value, simpleMemory, addresses, s);
            }));
        }
    }

    private void generateDefect(boolean value, Memory memory, List<Integer> addresses, String s) {
        for (int j = 0; j < addresses.size(); j += k) {
            Cell targetCell = memory.get(addresses.get(j));
            List<Cell> influenceCells = new ArrayList<>();

            ANPSFBaseCell baseCell = new ANPSFBaseCell(targetCell, s, influenceCells, value);

            for (int l = 0; l < k - 1; l++) {
                Cell cell = memory.get(addresses.get(j + l));
                ANPSFInfluenceCell influenceCell = new ANPSFInfluenceCell(cell, baseCell);
                influenceCells.add(influenceCell);
                memory.set(addresses.get(j + l), influenceCell);
            }
            memory.set(addresses.get(j), baseCell);
        }
    }

    @Override
    protected int getMultiplier() {
        return k;
    }

    private String toValue(int i) {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < (k - 1); j++) {
            sb.append((i >> j) & 1);
        }
        return sb.toString();
    }

    private static String toValue(List<Cell> cells) {
        StringBuilder sb = new StringBuilder();
        for (Cell cell : cells) {
            sb.append(cell.read() ? 1 : 0);
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        return "ANPSFK" + k;
    }

    private static class ANPSFBaseCell implements Cell {

        private Cell cell;
        private String pattern;
        private List<Cell> influenceCell;
        private Boolean valueToSet;

        public ANPSFBaseCell(Cell cell, String pattern, List<Cell> influenceCell, Boolean valueToSet) {
            this.cell = cell;
            this.pattern = pattern;
            this.influenceCell = influenceCell;
            this.valueToSet = valueToSet;
        }

        private void notifyChanged() {
            String s = toValue(influenceCell);
            if(pattern.equals(s)) {
                cell.write(valueToSet);
            }
        }

        @Override
        public boolean read() {
            return cell.read();
        }

        @Override
        public void write(boolean value) {
            cell.write(value);
        }
    }

    private static class ANPSFInfluenceCell implements Cell {

        private Cell cell;
        private ANPSFBaseCell baseCell;

        public ANPSFInfluenceCell(Cell cell, ANPSFBaseCell baseCell) {
            this.cell = cell;
            this.baseCell = baseCell;
        }

        @Override
        public boolean read() {
            return cell.read();
        }

        @Override
        public void write(boolean value) {
            cell.write(value);
            baseCell.notifyChanged();
        }
    }
}
