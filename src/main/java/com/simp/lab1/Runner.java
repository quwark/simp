package com.simp.lab1;

import com.simp.lab2.Schema;
import com.simp.lab2.SchemaTestInfo;
import com.simp.lab2.Value;
import com.simp.lab2.graph.FunctionalNode;
import com.simp.lab2.graph.InputNode;
import com.simp.lab2.graph.Node;
import com.simp.lab2.op.Operand;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Runner {
    public static void main(String[] args) {
        Schema schema = new Schema();

        InputNode x1 = schema.createInputNode();
        InputNode x2 = schema.createInputNode();
        InputNode x3 = schema.createInputNode();
        InputNode x4 = schema.createInputNode();
        InputNode x5 = schema.createInputNode();
        InputNode x6 = schema.createInputNode();

        FunctionalNode f1 = schema.createFunctionalNode(Operand.AND, x1, x2);
        FunctionalNode f2 = schema.createFunctionalNode(Operand.OR_NOT, x4, x5);
        FunctionalNode f3 = schema.createFunctionalNode(Operand.AND_NOT, f2, x6);
        FunctionalNode f4 = schema.createFunctionalNode(Operand.AND, x3, f3); // AND_NOT
        FunctionalNode f5 = schema.createFunctionalNode(Operand.OR, f1, f4); //OR_NOT

    }



   /* private static boolean calcValues(Node node, Map<Integer, Value > valueMap) {
        if(node instanceof InputNode) {

        } else if(node instanceof FunctionalNode) {
            List<Node> inputs = ((FunctionalNode) node).getInputs();
            Operand operand = ((FunctionalNode) node).getOperand();
            List<Boolean> collect = inputs.stream().map(i -> calcValue(i, valueMap)).collect(Collectors.toList());
            return operand.calculate(collect);
        }
        throw new IllegalArgumentException("Node");
    }*/
}
