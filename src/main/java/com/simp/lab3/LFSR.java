package com.simp.lab3;

import java.util.ArrayList;
import java.util.List;

public class LFSR {

    private int size;
    private List<Integer> taps = new ArrayList<>();

    private final int seed;
    private long cycles;

    private int register;

    public LFSR(List<Integer> taps, int seed, int size) {
        this.taps = taps;
        this.seed = seed;
        this.size = size;
        this.register = seed;
    }

    public LFSR(List<Integer> taps, String seed) {
        this(taps, Integer.parseInt(seed, 2), seed.length());
    }


    public int step() {
        cycles++;

        int bit = register & 1;

        int newBit = (register >> (size - 1 - taps.get(0))) & 1;
        for (int i = 1; i < taps.size(); i++) {
            newBit ^= (register >> (size - 1 - taps.get(i))) & 1;
        }

        register >>= 1;
        if (newBit == 1) {
            register |= (1 << size - 1);
        } else {
            register &= ~(1 << size - 1);
        }

        return bit;
    }

    public int generate(int numBits) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < numBits; i++) {
            sb.append(step());
        }
        return Integer.parseInt(sb.toString(), 2);
    }

    public int getSeed() {
        return seed;
    }

    public long getCycles() {
        return cycles;
    }
}
