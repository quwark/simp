package com.simp.lab3;

import com.simp.lab2.NodeResult;
import com.simp.lab2.Schema;
import com.simp.lab2.graph.FunctionalNode;
import com.simp.lab2.graph.InputNode;
import com.simp.lab2.op.Operand;

import java.util.*;
import java.util.stream.Collectors;


//seed = 5
public class Runner {

    private static final List<Integer> ALG = Collections.unmodifiableList(Arrays.asList(3, 4, 5, 7));
    private static final int LFSR_BITS = 8;

    public static void main(String[] args) {
        Schema schema = new Schema();

        InputNode x1 = schema.createInputNode();
        InputNode x2 = schema.createInputNode();
        InputNode x3 = schema.createInputNode();
        InputNode x4 = schema.createInputNode();
        InputNode x5 = schema.createInputNode();
        InputNode x6 = schema.createInputNode();
        InputNode x7 = schema.createInputNode();

        FunctionalNode f1 = schema.createFunctionalNode(Operand.AND_NOT, x1, x2);
        FunctionalNode f2 = schema.createFunctionalNode(Operand.NOT, x3);
        FunctionalNode f3 = schema.createFunctionalNode(Operand.OR_NOT, x5, x6);
        FunctionalNode f4 = schema.createFunctionalNode(Operand.AND, x4, f3, x7); // AND_NOT
        FunctionalNode f5 = schema.createFunctionalNode(Operand.OR, f2, f4); //OR_NOT
        FunctionalNode f6 = schema.createFunctionalNode(Operand.AND, f1, f5);

        List<FunctionalNode> functionalNodes = Arrays.asList(f1, f2, f3, f4, f5, f6);
        //List<FunctionalNode> functionalNodes = Arrays.asList(f6);

        int inputNodeCount = schema.getInputNodeCount();


        List<Stats> stats = new ArrayList<>();
        for (int i = 1; i < 128; i++) {
            Set<Integer> testSuits = schema.getUniqueInputs();
            LFSR lfsr = new LFSR(ALG, i, LFSR_BITS);

            int realityCheck = 5000;
            while (!testSuits.isEmpty() && realityCheck != 0) {
                int i1 = lfsr.generate(inputNodeCount);
                testSuits.remove(i1);
                realityCheck--;
            }

            stats.add(new Stats(lfsr.getSeed(), lfsr.getCycles(), realityCheck == 0, testSuits));
        }

        List<Stats> collect = stats.stream()
                .filter(s -> !s.isFailed())
                .sorted(Comparator.comparing(Stats::getCycles))
                .collect(Collectors.toList());
        collect.forEach(a -> System.out.println("seed = " + showStr(a.getSeed(), LFSR_BITS) + " === cycles (" + a.getCycles() / 7+ ")"));

        List<Stats> collect2 = stats.stream()
                .filter(Stats::isFailed)
                .sorted(Comparator.comparing(Stats::getCycles))
                .collect(Collectors.toList());
        collect2.forEach(a -> System.out.println("Failed seed = " + a.getSeed() + " === cycles (" + a.getCycles() + ")" + " Elems" + a.getElems()));

       /* LFSR lfsr = new LFSR(1, 8);
        for (int i = 0; i < 128; i++) {
            System.out.println(lfsr.generate(7));
        }*/
    }

    private static String showStr(int val, int size) {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < size; j++) {
            sb.append((val >> j) & 1);
        }
        return sb.reverse().toString();
    }

    private static class Stats {
        private int seed;
        private long cycles;
        private boolean failed;

        private Set<Integer> elems;

        public Stats(int seed, long cycles, boolean failed, Set<Integer> elems) {
            this.seed = seed;
            this.cycles = cycles;
            this.failed = failed;
            this.elems = elems;
        }

        public int getSeed() {
            return seed;
        }

        public long getCycles() {
            return cycles;
        }

        public boolean isFailed() {
            return failed;
        }

        public Set<Integer> getElems() {
            return elems;
        }
    }
}
