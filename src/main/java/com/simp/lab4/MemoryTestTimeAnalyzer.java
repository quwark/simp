package com.simp.lab4;

import com.simp.lab4.memory.Memory;
import com.simp.lab4.test.Tester;

import java.time.Duration;

public class MemoryTestTimeAnalyzer {

    private int frequency;

    public MemoryTestTimeAnalyzer(int frequency) {
        this.frequency = frequency;
    }

    public int getFrequency() {
        return frequency;
    }

    public Duration analyze(Memory memory, Tester tester) {
        long timeComplexity = tester.getNumberOfOperations(memory.getSize());
        return Duration.ofMillis(timeComplexity * 1000 / frequency) ;
    }
}
