package com.simp.lab4.test;

import com.simp.lab4.memory.Memory;

import java.util.HashSet;
import java.util.Set;

public class Walking01Tester implements Tester {

    private boolean testValue;

    public Walking01Tester() {
    }

    public Walking01Tester(boolean testValue) {
        this.testValue = testValue;
    }

    @Override
    public Set<Integer> execute(Memory memory) {
        Set<Integer> defects = new HashSet<>();

        long start = System.currentTimeMillis();
        for (int i = 0; i < memory.getSize(); i++) {
            memory.write(i, !testValue);
            for (int j = 0; j < memory.getSize(); j++) {
                if(i != j) {
                    memory.write(j, testValue);
                }
            }
            defects.addAll(checkMemory(memory, i));
            if(i % (memory.getSize() / 100) == 0) {
                long end = System.currentTimeMillis();
            }
        }

        return defects;
    }

    @Override
    public long getNumberOfOperations(long n) {
        return n * n;
    }

    private Set<Integer> checkMemory(Memory memory, int baseCell) {
        Set<Integer> defects = new HashSet<>();

        for (int i = 0; i < memory.getSize(); i++) {
            if(i != baseCell) {
                if(memory.read(i) != testValue) {
                    defects.add(i);
                }
            }
        }

        if(memory.read(baseCell) == testValue) {
            defects.add(baseCell);
        }

        return defects;
    }

    @Override
    public String toString() {
        return "Walking 1/0";
    }
}
