package com.simp.lab4.test;

import com.simp.lab4.memory.Memory;

import java.util.HashSet;
import java.util.Set;

public class MarchXTester implements Tester {


    @Override
    public Set<Integer> execute(Memory memory) {
        Set<Integer> defects = new HashSet<>();

        for (int i = 0; i < memory.getSize(); i++) {
            memory.write(i, false);
        }

        for (int i = 0; i < memory.getSize(); i++) {
            if(memory.read(i)) {
                defects.add(i);
            }
            memory.write(i, true);
        }

        for (int i = memory.getSize() - 1; i >= 0; i--) {
            if(!memory.read(i)) {
                defects.add(i);
            }
            memory.write(i, false);
        }

        for (int i = memory.getSize() - 1; i >= 0; i--) {
            if(memory.read(i)) {
                defects.add(i);
            }
        }

        return defects;
    }

    @Override
    public long getNumberOfOperations(long n) {
        return 6 * n;
    }

    @Override
    public String toString() {
        return "March X";
    }
}
