package com.simp.lab4.test;

import com.simp.lab4.memory.Memory;

import java.util.Set;

public interface Tester {

    Set<Integer> execute(Memory memory);

    long getNumberOfOperations(long n);
}
