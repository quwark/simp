package com.simp.lab4;

import com.simp.lab4.memory.Memory;
import com.simp.lab4.memory.SimpleMemory;
import com.simp.lab4.memory.defect.*;
import com.simp.lab4.test.MarchXTester;
import com.simp.lab4.test.Tester;
import com.simp.lab4.test.Walking01Tester;

import java.util.Arrays;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        ResultView.show(getDefectGenerators(), getTesters());

        Memory memory = new SimpleMemory(6 *  1024 * 1024);
        MemoryTestTimeAnalyzer testTimeAnalyzer = new MemoryTestTimeAnalyzer(1333_000_000);
        ResultView.show(memory, getTesters(), testTimeAnalyzer);
    }

    private static List<DefectGenerator> getDefectGenerators() {
        return Arrays.asList(new AFDefectGenerator(1000),
                new SAFDefectGenerator(1000),
                new CFidDefectGenerator(1000),
                new CFidDefectGenerator(1000));
    }

    private static List<Tester> getTesters() {
        return Arrays.asList(new Walking01Tester(), new MarchXTester());
    }
}
