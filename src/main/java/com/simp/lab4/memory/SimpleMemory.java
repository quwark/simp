package com.simp.lab4.memory;

import com.simp.lab4.memory.cell.Cell;
import com.simp.lab4.memory.cell.NormalCell;

public class SimpleMemory implements Memory {

    private final Cell[] cells;

    public SimpleMemory(int sizeMB) {
        this.cells = new Cell[sizeMB];
        for (int i = 0; i < cells.length; i++) {
            cells[i] = new NormalCell();
        }
    }

    @Override
    public boolean read(int index){
        return cells[index].read();
    }

    @Override
    public void write(int index, boolean value) {
        cells[index].write(value);
    }

    @Override
    public Cell get(int index) {
        return cells[index];
    }

    @Override
    public void set(int index, Cell cell) {
        cells[index] = cell;
    }

    @Override
    public int getSize() {
        return cells.length;
    }
}
