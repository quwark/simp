package com.simp.lab4.memory.cell;

public interface Cell {

    boolean read();

    void write(boolean value);
}
