package com.simp.lab4.memory.cell;

public class NormalCell implements Cell {

    private boolean value;

    @Override
    public boolean read() {
        return value;
    }

    @Override
    public void write(boolean value) {
        this.value = value;
    }
}
