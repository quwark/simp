package com.simp.lab4.memory;

import com.simp.lab4.memory.cell.Cell;

public interface Memory {

    boolean read(int index);

    void write(int index, boolean value);

    Cell get(int index);

    void set(int index, Cell cell);

    int getSize();
}
