package com.simp.lab4.memory.defect;

import com.simp.lab4.memory.Memory;

import java.util.Set;

public interface DefectGenerator {

    Set<Integer> generate(Memory memory, Set<Integer> cellsInUse);

    int getDefectCount();
}
