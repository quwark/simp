package com.simp.lab4.memory.defect;


import com.simp.lab4.memory.Memory;
import com.simp.lab4.memory.cell.Cell;

import java.util.Collections;
import java.util.List;

public class AFDefectGenerator extends DefectGeneratorBase {

    private static final int DEFAULT_ACCESSOR_NUMBER = 3;

    private final int numberOfAccessors;

    public AFDefectGenerator(int defectCount) {
        this(defectCount, DEFAULT_ACCESSOR_NUMBER);
    }

    public AFDefectGenerator(int defectCount, int numberOfAccessors) {
        super(defectCount);
        this.numberOfAccessors = numberOfAccessors;
    }

    @Override
    protected List<DefectType> getDefectTypes() {
        return Collections.singletonList(this::generateDefects);
    }

    private void generateDefects(Memory memory, List<Integer> addresses) {
        for (int i = 0; i < addresses.size(); i += 3) {
            Cell cell = memory.get(addresses.get(i));
            for (int j = 0; j < numberOfAccessors; j++) {
                memory.set(addresses.get(i + j), new AFCell(cell));
            }
        }
    }

    @Override
    protected int getMultiplier() {
        return numberOfAccessors;
    }

    @Override
    public String toString() {
        return "AF";
    }

    @Override
    public int getDefectCount() {
        return super.getDefectCount() * numberOfAccessors;
    }

    private static class AFCell implements Cell {
        private Cell cell;

        public AFCell(Cell cell) {
            this.cell = cell;
        }

        @Override
        public boolean read() {
            return cell.read();
        }

        @Override
        public void write(boolean value) {
            cell.write(value);
        }
    }
}
