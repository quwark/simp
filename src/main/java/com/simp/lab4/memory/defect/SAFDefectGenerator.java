package com.simp.lab4.memory.defect;

import com.simp.lab4.memory.Memory;
import com.simp.lab4.memory.cell.Cell;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SAFDefectGenerator extends DefectGeneratorBase {

    private static final List<DefectType> DEFECT_TYPE_LIST;

    static {
        DEFECT_TYPE_LIST = Collections.unmodifiableList(Arrays.asList(
                ((memory, addresses) -> generateDefects(memory, addresses, false)),
                ((memory, addresses) -> generateDefects(memory, addresses, true))
        ));
    }

    public SAFDefectGenerator(int defectCount) {
        super(defectCount);
    }

    @Override
    protected List<DefectType> getDefectTypes() {
        return DEFECT_TYPE_LIST;
    }

    private static void generateDefects(Memory memory, List<Integer> addresses, boolean value) {
        for (Integer address : addresses) {
            memory.set(address, new SAFCell(value));
        }
    }

    @Override
    public String toString() {
        return "SAF";
    }

    @Override
    protected int getMultiplier() {
        return 1;
    }

    private static class SAFCell implements Cell {

        private final boolean value;

        private SAFCell(boolean value) {
            this.value = value;
        }

        @Override
        public boolean read() {
            return value;
        }

        @Override
        public void write(boolean value) {

        }
    }
}
