package com.simp.lab4.memory.defect;

import com.simp.lab4.memory.Memory;
import com.simp.lab4.memory.cell.Cell;

import java.util.*;

public class CFinDefectGenerator extends DefectGeneratorBase {

    private static final List<DefectType> DEFECT_TYPE_LIST;

    static {
        DEFECT_TYPE_LIST = Collections.unmodifiableList(Arrays.asList(
                ((memory, addresses) -> {
                    addresses.sort(Comparator.naturalOrder());
                    generateDefects(memory, addresses, false);}),
                ((memory, addresses) -> {
                    addresses.sort(Comparator.naturalOrder());
                    generateDefects(memory, addresses, true);}),
                ((memory, addresses) -> {
                    addresses.sort(Comparator.reverseOrder());
                    generateDefects(memory, addresses, false);}),
                ((memory, addresses) -> {
                    addresses.sort(Comparator.reverseOrder());
                    generateDefects(memory, addresses, true);})
        ));
    }

    public CFinDefectGenerator(int defectCount) {
      super(defectCount);
    }

    @Override
    protected List<DefectType> getDefectTypes() {
        return DEFECT_TYPE_LIST;
    }

    @Override
    protected int getMultiplier() {
        return 2;
    }

    @Override
    public String toString() {
        return "CFin";
    }

    private static void generateDefects(Memory memory, List<Integer> addresses, boolean trigger) {
        for (int i = 0; i < addresses.size(); i += 2) {
            Cell aggressor = memory.get(addresses.get(i));
            Cell victim = memory.get(addresses.get(i + 1));
            memory.set(addresses.get(i), new AggressorCell(aggressor, victim, trigger));
        }
    }

    private static class AggressorCell implements Cell {
        private final Cell aggressor;
        private final Cell victim;
        private final boolean trigger;

        public AggressorCell(Cell aggressor, Cell victim, boolean trigger) {
            this.aggressor = aggressor;
            this.victim = victim;
            this.trigger = trigger;
        }

        @Override
        public boolean read() {
            return aggressor.read();
        }

        @Override
        public void write(boolean value) {
            if(value == trigger && aggressor.read() != value) {
                victim.write(!victim.read());
            }
            aggressor.write(value);
        }
    }
}
