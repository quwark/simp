package com.simp.lab4.memory.defect;

import com.simp.lab4.memory.Memory;

import java.util.List;

public interface DefectType {

    void create(Memory memory, List<Integer> addresses);
}
