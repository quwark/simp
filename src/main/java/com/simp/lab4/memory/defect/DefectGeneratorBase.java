package com.simp.lab4.memory.defect;

import com.simp.lab4.memory.Memory;

import java.util.*;
import java.util.stream.Collectors;

public abstract class DefectGeneratorBase implements DefectGenerator {

    protected final int defectCount;

    public DefectGeneratorBase(int defectCount) {
        this.defectCount = defectCount;
    }

    private List<Integer> generateRandomAddresses(Memory memory, int count, Set<Integer> cellsInUse) {
        return new Random()
                .ints(0, memory.getSize())
                .distinct()
                .filter(i -> !cellsInUse.contains(i))
                .limit(count)
                .boxed()
                .collect(Collectors.toList());
    }

    @Override
    public Set<Integer> generate(Memory memory, Set<Integer> cellsInUse) {
        List<Integer> addresses = generateRandomAddresses(memory, defectCount * getMultiplier(), cellsInUse);
        List<DefectType> defectTypes = getDefectTypes();

        int addressesPerType = defectCount / defectTypes.size() *  getMultiplier();
        int lastDefectCount = defectCount * getMultiplier() - (defectTypes.size() - 1) * addressesPerType;

        int skip = 0;
        for (int j = 0; j < defectTypes.size(); j++) {
            int limit = addressesPerType;
            if (j == defectTypes.size() - 1) {
                limit = lastDefectCount;
            }

            List<Integer> addrsPerType = addresses.stream().skip(skip).limit(limit).collect(Collectors.toList());
            skip += limit;

            defectTypes.get(j).create(memory, addrsPerType);
        }

        return new HashSet<>(addresses);
    }

    protected abstract List<DefectType> getDefectTypes();

    protected abstract int getMultiplier();

    @Override
    public int getDefectCount() {
        return defectCount;
    }
}
