package com.simp.lab4.memory.defect;

import com.simp.lab4.memory.Memory;
import com.simp.lab4.memory.cell.Cell;

import java.util.*;

public class CFidDefectGenerator extends DefectGeneratorBase {

    private static final List<DefectType> DEFECT_TYPE_LIST;

    static {
        DEFECT_TYPE_LIST = Collections.unmodifiableList(Arrays.asList(
                ((memory, addresses) -> {
                    addresses.sort(Comparator.naturalOrder());
                    generateDefects(memory, addresses, false, false);}),
                ((memory, addresses) -> {
                    addresses.sort(Comparator.naturalOrder());
                    generateDefects(memory, addresses, true, false);}),
                ((memory, addresses) -> {
                    addresses.sort(Comparator.naturalOrder());
                    generateDefects(memory, addresses, false, true);}),
                ((memory, addresses) -> {
                    addresses.sort(Comparator.naturalOrder());
                    generateDefects(memory, addresses, true, true);}),
                ((memory, addresses) -> {
                    addresses.sort(Comparator.reverseOrder());
                    generateDefects(memory, addresses, false, false);}),
                ((memory, addresses) -> {
                    addresses.sort(Comparator.reverseOrder());
                    generateDefects(memory, addresses, true, false);}),
                ((memory, addresses) -> {
                    addresses.sort(Comparator.reverseOrder());
                    generateDefects(memory, addresses, false, true);}),
                ((memory, addresses) -> {
                    addresses.sort(Comparator.reverseOrder());
                    generateDefects(memory, addresses, true, true);})
        ));
    }


    public CFidDefectGenerator(int defectCount) {
        super(defectCount);
    }

    @Override
    protected List<DefectType> getDefectTypes() {
        return DEFECT_TYPE_LIST;
    }

    @Override
    protected int getMultiplier() {
        return 2;
    }

    @Override
    public String toString() {
        return "CFid";
    }

    private static void generateDefects(Memory memory, List<Integer> addrs, boolean trigger, boolean value) {
        for (int i = 0; i < addrs.size(); i += 2) {
            Cell aggressor = memory.get(addrs.get(i));
            Cell victim = memory.get(addrs.get(i + 1));
            memory.set(addrs.get(i), new AggressorCell(aggressor, victim, trigger, value));
        }
    }

    private static class AggressorCell implements Cell {

        private final Cell aggressor;
        private final Cell victim;
        private final boolean trigger;
        private final boolean value;

        public AggressorCell(Cell aggressor, Cell victim, boolean trigger, boolean value) {
            this.aggressor = aggressor;
            this.victim = victim;
            this.trigger = trigger;
            this.value = value;
        }

        @Override
        public boolean read() {
            return aggressor.read();
        }

        @Override
        public void write(boolean value) {
            if(value == trigger && aggressor.read() != value) {
                victim.write(this.value);
            }
            aggressor.write(value);
        }
    }
}
