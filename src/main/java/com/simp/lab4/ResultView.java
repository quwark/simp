package com.simp.lab4;

import com.simp.lab4.MemoryTestTimeAnalyzer;
import com.simp.lab4.memory.Memory;
import com.simp.lab4.memory.SimpleMemory;
import com.simp.lab4.memory.defect.DefectGenerator;
import com.simp.lab4.test.Tester;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public interface ResultView {

    static void show(List<DefectGenerator> defectGenerators, List<Tester> testers) {
        System.out.println("Tests coverage:");
        for (DefectGenerator generator : defectGenerators) {
            Memory memory = new SimpleMemory(60 * Constants.MBIT_TO_BIT);
            generator.generate(memory, new HashSet<>());

            int defectCount = generator.getDefectCount();
            System.out.println("***************** Defect " + generator.toString() + " (Count " + defectCount + ")");
            for(Tester tester : testers) {
                Set<Integer> execute = tester.execute(memory);
                System.out.println(tester + " = " + String.format("%4.2f%%", execute.size() * 100. / defectCount));
            }
        }
    }

    static void show(Memory memory, List<Tester> testers, MemoryTestTimeAnalyzer testTimeAnalyzer) {
        System.out.println("Test Time for Memory " + memory.getSize() + " bit");
        System.out.println("Memory frequency " + testTimeAnalyzer.getFrequency() + " Hz");
        for(Tester tester : testers) {
            Duration duration = testTimeAnalyzer.analyze(memory, tester);
            System.out.println(tester.toString() + " = " + duration.toMillis() + " ms");
        }
    }
}
